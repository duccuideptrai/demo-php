<?php
namespace Deployer;

require 'recipe/laravel.php';

// Config
set('git_ssh_command', 'ssh');
//set('ssh_type', 'native');
set('repository', 'git@bitbucket.org:duccuideptrai/demo-php.git');

add('shared_files', []);
add('shared_dirs', []);
add('writable_dirs', []);

// Hosts
host('staging')
    ->setHostname('stiotnak.duckdns.org')
    ->setSshArguments(['-o UserKnownHostsFile=/dev/null', '-o StrictHostKeyChecking=no'])
    ->set('branch', 'develop')
    ->set('remote_user', 'ec2-user')
    ->set('deploy_path', '~/demo-php-staging');

host('production')
    ->setHostname('stiotnak.duckdns.org')
    ->setSshArguments(['-o UserKnownHostsFile=/dev/null', '-o StrictHostKeyChecking=no'])
    ->set('branch', 'master')
    ->set('remote_user', 'ec2-user')
    ->set('deploy_path', '~/demo-php-production');

// Hooks

after('deploy:failed', 'deploy:unlock');
